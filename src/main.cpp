#pragma comment(lib, "crypt32")
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "advapi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")

// Enable deprecated fopen - I want this to be compatible with Linux compilers
// TODO: only enable this when compiling on Linux, and use fopen_s for compilation on Windows
#define _CRT_SECURE_NO_DEPRECATE

#include <iostream>
#include <stdio.h>
#include <filesystem>
#include <string>
#include <openssl/evp.h>
#include <iomanip>
#include <sstream>
#include <map>
#include <vector>
#include <stack>
#include <forward_list>
#include <memory>
// #include <sqlite3.h>

//g++-9 -std=c++17 -DosWINDOWS main.cpp -l crypto -l ssl

namespace fs = std::filesystem;

using fsmap = std::map<std::string, std::vector<std::string>>;
//using fsmap = std::map<std::string, std::forward_list<std::string>>;

namespace Vornskr
{

class fsMapper
{
public:
	static std::shared_ptr< fsMapper > New()
	{
		std::shared_ptr<fsMapper> fsMap = std::make_shared<fsMapper>();
		fsMap->onCreate();
		return std::move(fsMap);
	}
	
	void add(std::string key, std::string entry)
	{
		data[key].push_back(entry);
		//data[key].push_front(entry);
	}
	
	std::vector<std::string> getValue(std::string key)
	//std::forward_list<std::string> getValue(std::string key)
	{
		return data[key];
	}

	void printDuplicates()
	{
		fsmap::iterator it;
		for (it = data.begin(); it != data.end(); ++it) {
			std::cout << it->first << "\n";
			for (std::string val : it->second) {
				std::cout << "\t" << val << "\n";
			}
		}
	}

	fsMapper() = default;
	~fsMapper() = default;
protected:
	virtual void onCreate() {}

	fsmap data;
};

class displayWindow
{
	void drawFileParams(fsMapper fileMap) {
		
	}

	void drawProcessParams() {}

	void drawTopology() {}
};

}

using namespace Vornskr;

/**
* hashFile
* 
* Function that returns sha256 hash of target file
* 
* TODO:
*	- Make generic (allow user to specify hashing method)
*	- Error handling
*	- swap char for fs::path?
*/
std::string hashFile(fs::directory_entry targ)
{
	unsigned char hash[EVP_MAX_MD_SIZE];
	unsigned char chunk[EVP_MAX_BLOCK_LENGTH];
	unsigned int hashLength = 0;
	//unsigned int bytes;
	size_t bytes;

	EVP_MD_CTX* context = EVP_MD_CTX_new();
	if (context == NULL) {
		std::cerr << "[ERROR] Unable to create hashing context" << "\n";
		std::exit(1);
		//throw "Unable to create hashing context";
	}

	FILE *targetFile = std::fopen(targ.path().string().c_str(), "rb");
	if (targetFile == NULL) {
		//std::cerr << "[WARNING] Unable to open file: " << targ.path().string().c_str() << "\n";
		EVP_MD_CTX_free(context);
		//std::exit(1);
		//throw "Unable to open file: " + targ.path().string();
		return "NULL: FILE COULD NOT BE OPENED";
	}
	else {
		EVP_DigestInit_ex(context, EVP_sha256(), NULL);
		while (bytes = (fread(chunk, 1, EVP_MAX_BLOCK_LENGTH, targetFile)))
		{
			EVP_DigestUpdate(context, chunk, bytes);
		}
		EVP_DigestFinal_ex(context, hash, &hashLength);

		std::stringstream ss;
		for (unsigned int i = 0; i < hashLength; ++i)
		{
			ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
		}

		EVP_MD_CTX_free(context);

		return ss.str();
	}
}

/**
* recursiveTree
* 
* Function that recursively lists directory contents - hashes regular files in the directory and recurses on subdirectories
* 
* TODO:
*	- Add a maximum depth
*	- Error handling
*/
void recursiveTree(const char* path, std::shared_ptr<fsMapper> fileMap)
{
	std::stringstream output;
	std::string fpath;
	std::stack<std::string> d_queue;

	d_queue.push(path);

	while (!d_queue.empty()) {
		fpath = d_queue.top();
		d_queue.pop();
		for (const auto& entry : fs::directory_iterator(fpath)) {
			if (entry.is_regular_file() && entry.file_size() > 0)
			{
				//std::cout << entry.path().string() << std::endl ;
				try {
					fileMap->add(hashFile(entry), entry.path().string());
				}
				catch (const char* err) {
					std::cerr << "[ERROR]\t" << err << "\n";
				}
			}

			else if (entry.is_directory())
			{
				d_queue.push(entry.path().string());
			}
		}
	}

	// Recursive version, awaiting deletion once my paranoia dies down
	//for (const auto& entry : fs::directory_iterator(path)) {
	//	if (entry.is_regular_file() && entry.file_size() > 0)
	//	{
	//		//std::cout << entry.path().string() << std::endl ;
	//		try {
	//			fileMap->add(hashFile(entry), entry.path().string());
	//		}
	//		catch (const char* err) {
	//			std::cerr << "[ERROR]\t" << err << "\n";
	//		}
	//	}

	//	else if (entry.is_directory())
	//	{
	//		recursiveTree(entry.path().string().c_str(), fileMap);
	//	}	
	//}
}

int main(int argc, char* argv[])
{
	auto fileMap = fsMapper::New();
	recursiveTree(argv[1], fileMap);
	fileMap->printDuplicates();
	return 0;
}
